package fr.rl.resources;

import fr.rl.dto.CouleurDTO;
import fr.rl.entities.CouleurEntity;
import fr.rl.repositories.CouleurRepository;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.*;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import java.util.List;

@Path("/couleurs/")
@Tag(name = "Couleur")
@Produces(MediaType.APPLICATION_JSON)
public class CouleurResource {
    @Inject
    private CouleurRepository couleurRepository;

    @GET
    public Response getAll() {
        List<CouleurEntity> couleurEntities = couleurRepository.listAll();
        return Response.ok(CouleurDTO.toDTOList(couleurEntities)).build();
    }

    @GET
    @Operation(summary = "Color by Id", description = "Search a color by its ID")
    @APIResponse(responseCode = "200", description = "OK, color found.")
    @APIResponse(responseCode = "404", description = "Color not found.")
    @Path("{id}")
    public Response getById(@PathParam("id") Integer id) {
        CouleurEntity couleurEntity = couleurRepository.findById(id);
        if (couleurEntity == null)
            return Response.status(404, "Cet identifiant n'existe pas !").build();
        else
            return Response.ok(new CouleurDTO(couleurEntity)).build();
    }

    @Transactional
    @POST
    public Response create(String nomCouleur, @Context UriInfo uriInfo) {
        CouleurEntity couleurEntity = new CouleurEntity();
        couleurEntity.setNom(nomCouleur);
        couleurRepository.persist(couleurEntity);
        UriBuilder uriBuilder = uriInfo.getRequestUriBuilder();
        uriBuilder.path(couleurEntity.getId().toString());
        return Response.created(uriBuilder.build()).build();
    }

    @Transactional
    @DELETE
    @APIResponse(responseCode = "204", description = "No Content: DELETE succeeded !")
    @APIResponse(responseCode = "404", description = "Color not found.")
    @APIResponse(responseCode = "400", description = "There is no ID.")
    @Path("{id}")
    public Response delete(@PathParam("id") Integer id) {
        if (id == null)
            return Response.status(Response.Status.BAD_REQUEST).build();
        if (couleurRepository.deleteById(id))
            return Response.status(Response.Status.NO_CONTENT).build();
        return Response.status(Response.Status.NOT_FOUND).build();
    }

    @Transactional
    @PUT
    @APIResponse(responseCode = "200", description = "OK, color created.")
    @APIResponse(responseCode = "409", description = "This resource would conflict with an existing resource")
    @APIResponse(responseCode = "400", description = "Request behavior error.")
    @Path("{id}")
    public Response update(@PathParam("id") Integer id, CouleurDTO couleurDTO) {
        if (id == null || couleurDTO == null)
            return Response.status(Response.Status.BAD_REQUEST).build();
        if (!id.equals(couleurDTO.getId())) {
            return Response.status(Response.Status.CONFLICT).entity(couleurDTO).build();
        }

        CouleurEntity savedCouleur = couleurRepository.findById(id);
        if(savedCouleur == null)
            return Response.status(Response.Status.NOT_FOUND).build();
        savedCouleur.setNom(couleurDTO.getNom());
        return Response.ok(new CouleurDTO(savedCouleur)).build();
        /*if (couleurRepository.getEntityManager().merge(couleurEntity) != null)
            return Response.ok(couleurEntity).build();
        else
            return Response.status(Response.Status.NOT_FOUND).build();*/
    }
}
